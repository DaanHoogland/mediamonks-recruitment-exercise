package io.hoogland.recruitmentexercise.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class City {
    private int id;
    private String name;
    private String country;
    @JsonProperty("coord")
    private Coordinates coordinates;
}