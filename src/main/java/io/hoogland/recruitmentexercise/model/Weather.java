package io.hoogland.recruitmentexercise.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.hoogland.recruitmentexercise.model.deserializer.WeatherDeserializer;
import lombok.Data;

import java.sql.Timestamp;

@Data
@JsonDeserialize(using = WeatherDeserializer.class)
public class Weather {
    private int id;
    private String name;
    private String condition;
    private String description;
    private double temp;
    private double minTemp;
    private double maxTemp;
    private String unit;
    private int humidity;
    private Timestamp savedOn;
}