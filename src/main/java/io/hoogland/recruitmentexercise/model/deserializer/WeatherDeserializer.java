package io.hoogland.recruitmentexercise.model.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import io.hoogland.recruitmentexercise.model.Weather;

import java.io.IOException;
import java.sql.Timestamp;

public class WeatherDeserializer extends StdDeserializer<Weather> {

    public WeatherDeserializer() {
        super(Weather.class);
    }

    /**
     * Custom deserializer for the weather object
     *
     * @param jsonParser             Jackson json parser
     * @param deserializationContext context for the process of deserialization a single root-level value
     * @return deserialized {@link Weather} object
     * @throws IOException
     * @throws JsonProcessingException
     * @see <a href="https://openweathermap.org/current#current_JSON">OpenWeatherMap JSON format</a>
     * @see <a href="https://github.com/FasterXML/jackson-docs">Jackson docs</a>
     */
    @Override
    public Weather deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Weather weather = new Weather();
        JsonNode jNode = jsonParser.getCodec().readTree(jsonParser);

        if(!jNode.has("city")) {
            weather.setId(jNode.get("id").asInt());
            weather.setName(jNode.get("name").textValue());
        } else {
            weather.setId(jNode.get("city").get("id").asInt());
            weather.setName(jNode.get("city").get("name").textValue());

        }
        weather.setCondition(jNode.get("weather").get(0).get("main").textValue());
        weather.setDescription(jNode.get("weather").get(0).get("description").textValue());
        weather.setTemp(jNode.get("main").get("temp").asDouble());
        weather.setMinTemp(jNode.get("main").get("temp_min").asDouble());
        weather.setMaxTemp(jNode.get("main").get("temp_max").asDouble());
        weather.setHumidity(jNode.get("main").get("humidity").asInt());

        return weather;
    }
}
