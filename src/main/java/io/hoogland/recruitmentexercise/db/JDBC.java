package io.hoogland.recruitmentexercise.db;

import io.hoogland.recruitmentexercise.helper.StorageHelper;
import lombok.extern.java.Log;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

@Log
public class JDBC {

    public static final String DATABASE_WEATHER = "weather";
    private static final String OS = System.getProperty("os.name");
    private static final String DATABASE_NAME = "recruitment.db";
    private static final String CREATE_WEATHER_TABLE = "CREATE TABLE IF NOT EXISTS weather (id int NOT NULL, name text NOT NULL, " +
            "`condition` text NOT NULL, description text NOT NULL, temp decimal(3,2) NOT NULL, " +
            "min_temp decimal(3,2) NOT NULL, max_temp decimal(3,2) NOT NULL, unit text NOT NULL, humidity int NOT NULL, " +
            "saved_on TIMESTAMP NOT NULL, PRIMARY KEY(id));";
    private static Connection connection = null;

    /**
     * Starts the initial connection with the sqlite database.
     * <p>Creates the sqlite database if it isn't present and loads up the weather schema
     * found in {@link JDBC#CREATE_WEATHER_TABLE}</p>
     *
     * @throws SQLException thrown when no connection can be made to the database
     *                      or the method was unable to create the schema.
     */
    private static void initiateConnection() throws SQLException {
        // Build a url for the JDBC connection.
        String homePath = StorageHelper.getOfflineDataPath();
        StorageHelper.createPath(homePath);
        // Add proper backslashes for Windows/*nix paths
        if (OS.contains("Win")) {
            homePath += "\\" + DATABASE_NAME;
        } else {
            homePath += "/" + DATABASE_NAME;
        }
        String url = "jdbc:sqlite:" + homePath;
        boolean databaseExists = new File(homePath).exists();

        connection = DriverManager.getConnection(url);

        // Create tables only if it's the first time running the application to avoid running it on every startup.
        if (!databaseExists) {
            try (Statement statement = connection.createStatement()) {
                statement.execute(CREATE_WEATHER_TABLE);
            }
        }

    }

    /**
     * Returns a static instance of the database connection.
     *
     * @return a Connection object to connect to the database
     * @throws SQLException thrown if no connection could be made
     */
    public static Connection getConnection() throws SQLException {
        if (connection == null) {
            initiateConnection();
        }
        return connection;
    }

    /**
     * Close the database connection when it's not needed anymore.
     */
    public static void closeConnection() {
        if (connection == null) {
            log.info("No open connections");
        } else {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                log.severe("Failed to close open connection:\n" + Arrays.toString(e.getStackTrace()));
            }
        }
    }
}
