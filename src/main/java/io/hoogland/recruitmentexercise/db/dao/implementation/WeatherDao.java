package io.hoogland.recruitmentexercise.db.dao.implementation;

import io.hoogland.recruitmentexercise.db.JDBC;
import io.hoogland.recruitmentexercise.db.dao.Dao;
import io.hoogland.recruitmentexercise.model.Weather;

import java.sql.*;
import java.util.Optional;

public class WeatherDao implements Dao<Weather> {

    private final String SELECT = "SELECT * FROM %s WHERE id=?";
    private final String INSERT = "INSERT INTO %s VALUES(?,?,?,?,?,?,?,?,?,?)";
    private final String UPDATE = "UPDATE %s SET name=?, `condition`=?, description=?, " +
            "temp=?, min_temp=?, max_temp=?, unit=?, humidity=?, saved_on=? WHERE id=?";
    private final String DELETE = "DELETE FROM %s WHERE id=?";

    /**
     * Returns a Weather object from the sqlite database if it exists.
     *
     * @param id the id of the weather object (shared with City object)
     * @return the found weather object. Empty if no results are found.
     * @throws SQLException thrown when there is an error in the query or
     *                      no connection could be established.
     */
    @Override
    public Optional<Weather> get(int id) throws SQLException {
        Connection conn = JDBC.getConnection();

        // Prepare a select statement.
        PreparedStatement selectStatement = conn.prepareStatement(String.format(SELECT, JDBC.DATABASE_WEATHER));
        selectStatement.setInt(1, id);
        ResultSet results = selectStatement.executeQuery();

        // If result set contains results, return the first
        if (results.next()) {
            Weather weather = new Weather();
            weather.setId(results.getInt("id"));
            weather.setName(results.getString("name"));
            weather.setCondition(results.getString("condition"));
            weather.setDescription(results.getString("description"));
            weather.setTemp(results.getDouble("temp"));
            weather.setMinTemp(results.getDouble("min_temp"));
            weather.setMaxTemp(results.getDouble("max_temp"));
            weather.setUnit(results.getString("unit"));
            weather.setHumidity(results.getInt("humidity"));
            weather.setSavedOn(results.getTimestamp("saved_on"));

            return Optional.of(weather);
        }

        // If result set contains no results, return empty optional object.
        return Optional.empty();
    }

    /**
     * Save a weather object to the sqlite database.
     * <p>Swaps to the {@link WeatherDao#update(Weather)} method if a weather with the same ID already exists</p>
     *
     * @param weather the object to be saved
     * @throws SQLException thrown when there is an error in the query or
     *                      no connection could be established.
     */
    @Override
    public void save(Weather weather) throws SQLException {
        Connection conn = JDBC.getConnection();

        // Double check if there is no existing entry.
        Optional<Weather> optionalWeather = get(weather.getId());
        if (!optionalWeather.isPresent()) {
            PreparedStatement insertStatement = conn.prepareStatement(String.format(INSERT, JDBC.DATABASE_WEATHER));
            insertStatement.setInt(1, weather.getId());
            insertStatement.setString(2, weather.getName());
            insertStatement.setString(3, weather.getCondition());
            insertStatement.setString(4, weather.getDescription());
            insertStatement.setDouble(5, weather.getTemp());
            insertStatement.setDouble(6, weather.getMinTemp());
            insertStatement.setDouble(7, weather.getMaxTemp());
            insertStatement.setString(8, weather.getUnit());
            insertStatement.setInt(9, weather.getHumidity());
            insertStatement.setTimestamp(10, new Timestamp(System.currentTimeMillis()));

            insertStatement.execute();
        } else {
            update(weather);
        }
    }

    /**
     * Updates a weather object in the sqlite database.
     *
     * @param weather the object with the updated values
     * @throws SQLException thrown when there is an error in the query or
     *                      no connection could be established.
     */
    @Override
    public void update(Weather weather) throws SQLException {
        Connection conn = JDBC.getConnection();

        PreparedStatement updateStatement = conn.prepareStatement(String.format(UPDATE, JDBC.DATABASE_WEATHER));
        updateStatement.setString(1, weather.getName());
        updateStatement.setString(2, weather.getCondition());
        updateStatement.setString(3, weather.getDescription());
        updateStatement.setDouble(4, weather.getTemp());
        updateStatement.setDouble(5, weather.getMinTemp());
        updateStatement.setDouble(6, weather.getMaxTemp());
        updateStatement.setString(7, weather.getUnit());
        updateStatement.setInt(8, weather.getHumidity());
        updateStatement.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
        updateStatement.setInt(10, weather.getId());

        updateStatement.execute();
    }

    /**
     * Deletes a weather object from the sqlite database by weather object.
     *
     * @param weather the object to be deleted
     * @throws SQLException thrown when there is an error in the query or
     *                      no connection could be established.
     */
    @Override
    public void delete(Weather weather) throws SQLException {
        delete(weather.getId());
    }

    /**
     * Deletes a weather object from the sqlite database using the ID
     *
     * @param id the id of the row that will be deleted.
     * @throws SQLException thrown when there is an error in the query or
     *                      no connection could be established.
     */
    @Override
    public void delete(int id) throws SQLException {
        Connection conn = JDBC.getConnection();

        PreparedStatement deleteStatement = conn.prepareStatement(String.format(DELETE, JDBC.DATABASE_WEATHER));
        deleteStatement.setInt(1, id);
        deleteStatement.execute();
    }
}
