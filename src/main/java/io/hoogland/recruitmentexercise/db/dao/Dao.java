package io.hoogland.recruitmentexercise.db.dao;

import java.sql.SQLException;
import java.util.Optional;

public interface Dao<T> {

    Optional<T> get(int id) throws SQLException;

    void save(T t) throws SQLException;

    void update(T t) throws SQLException;

    void delete(T t) throws SQLException;

    void delete(int id) throws SQLException;

}
