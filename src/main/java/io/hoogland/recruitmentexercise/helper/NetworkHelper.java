package io.hoogland.recruitmentexercise.helper;

import java.net.URL;
import java.net.URLConnection;

public class NetworkHelper {

    /**
     * Helper class to check for internet connection
     * <p>Checks the connection to openweathermap.org, since that's the only relevant
     * internet connection made in the application.</p>
     *
     * @return whether or not the application is able to connect to openweathermap.org.
     */
    public static boolean isConnected() {
//        return false;
        try {
            URL url = new URL("https://openweathermap.org");
            URLConnection connection = url.openConnection();
            connection.connect();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
