package io.hoogland.recruitmentexercise.helper;

import io.hoogland.recruitmentexercise.model.City;
import io.hoogland.recruitmentexercise.model.Weather;

import java.util.List;

public class OutputHelper {
    private static final String CITY_OUTPUT = "City name: %s\nCountry code: %s\nCoordinates:\n\tLongtitude: %10.8f\n" +
            "\tLatitude: %10.8f";
    private static final String WEATHER_OUTPUT = "\n\nCurrent condition: %s\n\t\t%s\nTemperature: %2.1f%s\n" +
            "\t\t(min: %2.1f%s, max: %2.1f%s)\nHumidity: %d";
    private static final String CITY_SELECTION_HEADER = "Multiple cities found with name: %s\n";
    private static final String CITY_SELECTION_FOOTER = "Number of correct city: ";
    private static final String CITY_SELECTION = "[%d] %s, %s\t\tLon: %10.8f Lat: %10.8f\n";
    public static final String HELP = "Parameters accepted by the application:\n" +
            "\t-c, --city <param>\t\tThe city of which information will be displayed\n" +
            "\t-w, --weather\t\t\tFlag to display weather as well as city information. Accepts no parameters.\n" +
            "\t-u, --units <param>\t\tThe units the temperature is displayed in.\n" +
            "\t\t\t\t\t\t\tChoose from metric, imperial, default\n" +
            "\t-a, --api <param>\t\tTo use a different API key.\n" +
            "\t-h, --help\t\t\t\tShows this message.\n\n" +
            "Example usage:\njava -jar <path-to-jar> -c Amsterdam -w";

    /**
     * Gets the formatted String to show general information about a city
     *
     * @param city the {@link City} object containing the information
     * @return the formatted String ready to print
     */
    public static String getCityOutput(City city) {
        return String.format(CITY_OUTPUT, city.getName(), city.getCountry(),
                city.getCoordinates().getLongitude(), city.getCoordinates().getLatitude());
    }

    /**
     * Gets the formatted String to show weather information about a city
     *
     * @param city    the {@link City} object that the {@link Weather} belongs to
     * @param weather the {@link Weather} object containing weather information
     * @return the formatted String ready to print
     */
    public static String getWeatherOutput(City city, Weather weather) {
        String unitSymbol = getTemperatureUnitSymbol(weather.getUnit());
        return getCityOutput(city) + String.format(WEATHER_OUTPUT, weather.getCondition(), weather.getDescription(),
                weather.getTemp(), unitSymbol, weather.getMinTemp(), unitSymbol, weather.getMaxTemp(),
                unitSymbol, weather.getHumidity());
    }

    /**
     * Gets the formatted String to show a list of cities in an ordered list
     *
     * @param cityList a list of {@link City} objects with the same name
     * @param cityName the name of the city
     * @return the formatted String ready to print
     */
    public static String getCitySelection(List<City> cityList, String cityName) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(CITY_SELECTION_HEADER, cityName));
        for (int i = 0; i < cityList.size(); i++) {
            City city = cityList.get(i);
            builder.append(String.format(CITY_SELECTION, i + 1, city.getName(), city.getCountry(),
                    city.getCoordinates().getLongitude(), city.getCoordinates().getLatitude()));
        }
        builder.append(CITY_SELECTION_FOOTER);

        return builder.toString();
    }

    /**
     * Get the temperature unit symbol
     *
     * @param unit the unit in String format.
     * @return The corresponding temperature symbol
     */
    private static String getTemperatureUnitSymbol(String unit) {
        String unitSymbol;
        switch (unit.toUpperCase()) {
            case "METRIC":
                unitSymbol = "°C";
                break;
            case "IMPERIAL":
                unitSymbol = "°F";
                break;
            default:
                unitSymbol = "K";
                break;
        }
        return unitSymbol;
    }
}
