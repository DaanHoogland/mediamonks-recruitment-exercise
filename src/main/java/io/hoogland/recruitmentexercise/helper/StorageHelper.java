package io.hoogland.recruitmentexercise.helper;

import java.io.File;

public class StorageHelper {

    private static final String STORAGE_FOLDER = "recruitment";

    /**
     * Returns a path for the offline storage of cities depending on OS
     *
     * @param fileName whether or not the filename is included in the returned String
     * @return the path of the gzipped cities json file
     */
    public static String getOfflineDataPath(String fileName) {
        String os = System.getProperty("os.name");
        String homePath = System.getProperty("user.home");
        String dataPath;
        if (os.contains("Win")) {
            dataPath = homePath + "\\" + STORAGE_FOLDER;
            if (fileName != null) {
                dataPath += "\\" + fileName;
            }
        } else {
            dataPath = homePath + "/" + STORAGE_FOLDER;
            if (fileName != null) {
                dataPath += "/" + fileName;
            }
        }

        return dataPath;
    }

    public static String getOfflineDataPath() {
        return getOfflineDataPath(null);
    }

    /**
     * Creates the folder for offline data storage if it doesn't exist
     *
     * @param dataPath the path to the folder
     */
    public static void createPath(String dataPath) {
        File file = new File(dataPath);
        file.mkdir();
    }
}
