package io.hoogland.recruitmentexercise.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.hoogland.recruitmentexercise.db.JDBC;
import io.hoogland.recruitmentexercise.db.dao.implementation.WeatherDao;
import io.hoogland.recruitmentexercise.helper.NetworkHelper;
import io.hoogland.recruitmentexercise.helper.StorageHelper;
import io.hoogland.recruitmentexercise.model.City;
import io.hoogland.recruitmentexercise.model.Weather;
import io.hoogland.recruitmentexercise.runnable.DownloadRunnable;
import lombok.extern.java.Log;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.*;
import java.util.zip.GZIPInputStream;

@Log
public class OpenWeatherAPI {

    private static final String BULK_CITY_URL = "http://bulk.openweathermap.org/sample/city.list.json.gz";
    private static final String BULK_CITY_FILE = "city.list.json.gz";
    private static final String WEATHER_API_URL = "https://api.openweathermap.org/data/2.5/weather?id=%d&APPID=%s&units=%s";
    private static final String BULK_WEATHER_URL = "http://bulk.openweathermap.org/sample/weather_14.json.gz";
    private static final String BULK_WEATHER_FILE = "weather_14.json.gz";

    /**
     * Loads a bulk city sample from the OpenWeatherMap API.
     *
     * @return a HashMap containing cities, searchable by name
     */
    public static HashMap<String, List<City>> loadCityData() {
        try {
            // Load in data from gzipped json to String object
            GZIPInputStream gzipInputStream = getGzipInputStreamForUrl(BULK_CITY_URL, BULK_CITY_FILE);
            String value = IOUtils.toString(gzipInputStream, Charset.defaultCharset());
            gzipInputStream.close();

            // Deserialize the cities to POJO's
            ObjectMapper mapper = new ObjectMapper();
            List<City> cityList = mapper.readValue(value, new TypeReference<List<City>>() {
            });

            // Load all objects into a hashmap
            HashMap<String, List<City>> cityMap = new HashMap<>();
            for (City city : cityList) {
                if (cityMap.containsKey(city.getName().toUpperCase())) {
                    cityMap.get(city.getName().toUpperCase()).add(city);
                } else {
                    cityMap.put(city.getName().toUpperCase(), new ArrayList<>(Arrays.asList(city)));
                }
            }

            // Return loaded in data.
            return cityMap;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Generates a GZIPInputStream from a URL and saves the gzipped JSON locally.
     *
     * @param url             URL the gzipped JSON can be found at
     * @param offlineFileName the file name of the gzipped JSON when stored locally
     * @return a GZIPInputStream with the content of the URL
     * @throws IOException
     */
    private static GZIPInputStream getGzipInputStreamForUrl(String url, String offlineFileName) throws IOException {
        InputStream fileInputStream = null;
        GZIPInputStream gzipInputStream;
        if (NetworkHelper.isConnected()) {
            // Start separate thread, save the file.
            new Thread(new DownloadRunnable(url, offlineFileName)).start();
            // Stream meanwhile thread downloads it in background for future offline use.
            fileInputStream = new URL(url).openStream();
        } else {
            // Load offline data saved from previous run. If none is found throw error and exit.
            if (new File(StorageHelper.getOfflineDataPath(offlineFileName)).exists()) {
                fileInputStream = new FileInputStream(StorageHelper.getOfflineDataPath(offlineFileName));
            } else {
                System.err.println("No offline backup of data found.\nPlease connect to the internet.");
                JDBC.closeConnection();
                System.exit(1);
            }
        }

        // Load in data from gzipped json to String object.
        gzipInputStream = new GZIPInputStream(new BufferedInputStream(fileInputStream));
        return gzipInputStream;
    }

    /**
     * Downloads a bulk weather sample from the OpenWeatherMap API.
     *
     * @return a HashMap containing weather information, searchable by city ID
     */
    public static HashMap<Integer, Weather> getBulkWeatherData() {
        // Load in data from gzipped json to String object.
        GZIPInputStream gzipInputStream = null;
        try {
            gzipInputStream = getGzipInputStreamForUrl(BULK_WEATHER_URL, BULK_WEATHER_FILE);
            String value = IOUtils.toString(gzipInputStream, Charset.defaultCharset());
            gzipInputStream.close();

            // Parse json String data to POJO's.
            HashMap<Integer, Weather> weatherHashMap = new HashMap<>();
            MappingIterator<Weather> iterator = new ObjectMapper().readerFor(Weather.class).readValues(value);
            while (iterator.hasNext()) {
                Weather weatherValue = iterator.next();
                // Set default unit to display temperature in.
                weatherValue.setUnit("K");
                weatherHashMap.put(weatherValue.getId(), weatherValue);
            }

            return weatherHashMap;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Gets the weather for a given city.
     *
     * @param cityId the ID of the city
     * @param apiKey API key to be used for OpenWeatherAPI
     * @param units  the units the temperature is displayed in
     * @return a weather object containing weather for the given city
     */
    public static Weather getWeatherForCity(int cityId, String apiKey, String units) {
        WeatherDao dao = new WeatherDao();
        try {
            // If application can connect to internet, load live from the internet
            if (NetworkHelper.isConnected()) {
                // Open connection to web page
                URL url = new URL(String.format(WEATHER_API_URL, cityId, apiKey, units));
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                // Read web page lines and combine them in a String
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = reader.readLine()) != null) {
                    content.append(inputLine);
                }
                reader.close();
                urlConnection.disconnect();

                // Deserialize String to POJO
                Weather weather = new ObjectMapper().readValue(String.valueOf(content), Weather.class);
                weather.setUnit(units);
                try {
                    dao.save(weather);
                } catch (SQLException e) {
                    log.warning("Weather information couldn't be saved to the local database.");
                }
                return weather;
            } else {
                // If application can't connect to website, check local database for any backups
                Optional<Weather> weather = Optional.empty();
                try {
                    weather = dao.get(cityId);
                } catch (SQLException e) {
                    log.warning("Weather information could not be retrieved from the local database.");
                }

                // If there is a backup present, show that. If not, show error and exit.
                if (weather != null && weather.isPresent()) {
                    System.out.println("Showing latest offline backup from: " + weather.get().getSavedOn() + "\n");
                    return weather.get();
                } else {
                    return null;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
