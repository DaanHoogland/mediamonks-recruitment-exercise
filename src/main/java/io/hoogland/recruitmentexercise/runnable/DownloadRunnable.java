package io.hoogland.recruitmentexercise.runnable;

import io.hoogland.recruitmentexercise.helper.StorageHelper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class DownloadRunnable implements Runnable {

    private String url;
    private String fileName;

    public DownloadRunnable(String url, String fileName) {
        this.url = url;
        this.fileName = fileName;
    }

    /**
     * Thread to run in the background while the application streams the gzipped json file.
     * Downloads the gzipped file to the local computer and creates folders if necessary.
     */
    @Override
    public void run() {
        try {
            BufferedInputStream inputStream = new BufferedInputStream(new URL(url).openStream());
            StorageHelper.createPath(StorageHelper.getOfflineDataPath());
            Files.copy(inputStream, Paths.get(StorageHelper.getOfflineDataPath(fileName)), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
