package io.hoogland.recruitmentexercise.runnable;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import io.hoogland.recruitmentexercise.api.OpenWeatherAPI;
import io.hoogland.recruitmentexercise.db.JDBC;
import io.hoogland.recruitmentexercise.helper.OutputHelper;
import io.hoogland.recruitmentexercise.model.City;
import io.hoogland.recruitmentexercise.model.Weather;
import lombok.extern.java.Log;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;

@Log
public class WeatherRunnable implements Runnable {

    @Parameter(names = {"--cityParam", "-c"})
    private String cityParam = "";

    @Parameter(names = {"--weatherParam", "-w"})
    private boolean weatherParam = false;

    @Parameter(names = {"--units", "-u"})
    private String unitParam = "metric";

    @Parameter(names = {"--api", "-a"})
    private String apiKey = "ee823cb14ddbd6bf9bc43a9b329eb5b0";

    @Parameter(names = {"-h", "--help"})
    private boolean helpParam = false;

    private Future<HashMap<String, List<City>>> cityMap;
    private Future<HashMap<Integer, Weather>> weatherMap;

    /**
     * Constructor with argument array
     *
     * @param args arguments given to the application through the {@link io.hoogland.recruitmentexercise.Main#main(String[])} method
     */
    public WeatherRunnable(String[] args) {
        super();
        // Use library to handle arguments for easy expansion and to make it more readable.
        JCommander.newBuilder().addObject(this).build().parse(args);
    }

    @Override
    public void run() {
        // Show help text in case help parameter is flagged true.
        if (helpParam) {
            System.out.println(OutputHelper.HELP);
            System.exit(0);
        }

        // Attempt to establish database connection.
        openDbConnection();

        ExecutorService executorService = Executors.newCachedThreadPool();

        // Start loading bulk data on separate threads.
        cityMap = executorService.submit(OpenWeatherAPI::loadCityData);
        weatherMap = executorService.submit(OpenWeatherAPI::getBulkWeatherData);

        try {
            // Check if the city exists in the data set
            checkIfCityExists(cityParam);

            // Check if there are multiple cities with the same name as user input.
            if (cityMap.get().get(cityParam.toUpperCase()).size() > 1) {
                outputForMultipleCities(cityParam);
            } else {
                // If weather parameter is present, show weather, if not, show city details.
                outputForSpecificCity(cityMap.get().get(cityParam.toUpperCase()).get(0));
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Shut all still open threads down.
        executorService.shutdown();

        // Close db connection before application exits.
        JDBC.closeConnection();
    }

    /**
     * Sets up the database connection and shows error if it fails.
     */
    private void openDbConnection() {
        // Attempt to establish database connection.
        try {
            JDBC.getConnection();
        } catch (SQLException e) {
            log.warning("Continuing without database connection");
        }
    }

    /**
     * Checks if the city exists in the data set of cities downloaded in {@link OpenWeatherAPI#loadCityData()}
     *
     * @param cityParam the city name to be looked up
     */
    private void checkIfCityExists(String cityParam) throws ExecutionException, InterruptedException {
        // Check if the city actually exists in the set of data.
        if (!cityParam.trim().isEmpty()) {
            if (!cityMap.get().containsKey(cityParam.toUpperCase()) || cityMap.get().get(cityParam.toUpperCase()).isEmpty()) {
                log.severe("No city found with the name: " + cityParam);
                JDBC.closeConnection();
                System.exit(1);
            }
        } else {
            log.severe("Application needs a city name to function.");
            JDBC.closeConnection();
            System.exit(1);
        }

    }

    /**
     * Prints a selection of cities which the user can choose from, then prints weather for chosen city.
     *
     * @param cityParam the name of the city of which duplicates are found.
     */
    private void outputForMultipleCities(String cityParam) throws ExecutionException, InterruptedException {
        // Print selection options
        System.out.print(OutputHelper.getCitySelection(cityMap.get().get(cityParam.toUpperCase()), cityParam));

        // Allow user input
        Scanner scanner = new Scanner(System.in);
        int cityIndex = 0;
        try {
            cityIndex = scanner.nextInt() - 1;

            // Check if number is valid, if not wait for next input.
            while (cityIndex < 0 || cityIndex > cityMap.get().get(cityParam.toUpperCase()).size()) {
                System.err.println("Choose a number between 1 and " + cityMap.get().get(cityParam.toUpperCase()).size());
                cityIndex = scanner.nextInt() - 1;
            }
        } catch (InputMismatchException e) {
            System.err.println("Input not a number.\nDefaulting to first option.");
        }
        City city = cityMap.get().get(cityParam.toUpperCase()).get(cityIndex);
        outputForSpecificCity(city);
    }

    /**
     * Prints the weather for one specific city.
     *
     * @param city the city of which the weather will be displayed
     */
    private void outputForSpecificCity(City city) {
        System.out.println("===============================");
        // If weather parameter is present, show weather, if not, show city details.
        if (weatherParam) {
            Weather weather = OpenWeatherAPI.getWeatherForCity(city.getId(), apiKey, unitParam);
            // Make sure there is a weather object available either from using the online API or the backup database.
            if (weather != null) {
                System.out.println(OutputHelper.getWeatherOutput(city, weather));
            } else {
                // If no local weather information is available, look for it in the downloaded bulk file.
                Weather offlineWeather = null;
                try {
                    offlineWeather = weatherMap.get().get(city.getId());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }

                // Show error if city isn't found in the bulk file.
                if (offlineWeather == null) {
                    System.err.println("No offline weather information found.");
                } else {
                    System.out.println(OutputHelper.getWeatherOutput(city, offlineWeather));
                }
            }
        } else {
            // No weather flag, show generic city information.
            System.out.println(OutputHelper.getCityOutput(city));
        }
    }
}
