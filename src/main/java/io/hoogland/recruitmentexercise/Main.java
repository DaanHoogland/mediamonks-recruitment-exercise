package io.hoogland.recruitmentexercise;

import io.hoogland.recruitmentexercise.runnable.WeatherRunnable;

public class Main {
    public static void main(String[] args) {
        new WeatherRunnable(args).run();
    }
}
